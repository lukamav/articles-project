import os
import re

from django.test import TestCase

from users.models import *
from users.factories import UserFactory, ArticleFactory
from .factories import CommentFactory
from .models import *


def test_data():
    users = []
    for i in range(30):
        users.append(UserFactory())

    for i in range(5, 30):
        Relationship.objects.create(from_person=users[0].userprofile, to_person=users[i].userprofile, rel_status=1)

    articles = []
    for i in range(10):
        if len(articles) < 5:
            articles.append(ArticleFactory())
        else:
            articles.append(ArticleFactory(category="culture"))
    comments = []

    for u in users:
        comments.append(CommentFactory(article=articles[0], user=u))
    UserFactory()


class IndexViewTest(TestCase):
    def setUp(self):
        UserFactory.reset_sequence()  # so we have the same usernames throughout tests
        ArticleFactory.reset_sequence()
        CommentFactory.reset_sequence()
        test_data()

    def test_sport_articles(self):
        response = self.client.get(reverse("sth:index"))
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "sth/index.html")
        self.assertEqual(str(response.context["sport_articles"]),
                         "[<Article: Topic0>, <Article: Topic1>, <Article: Topic2>, <Article: Topic3>]")
        self.assertEqual(str(response.context["culture_articles"]),
                         "[<Article: Topic5>, <Article: Topic6>, <Article: Topic7>, <Article: Topic8>]")


class ArticlesViewTest(TestCase):
    def setUp(self):
        UserFactory.reset_sequence()  # so we have the same usernames throughout tests
        ArticleFactory.reset_sequence()
        CommentFactory.reset_sequence()
        test_data()

    def tearDown(self):
        image_dir = os.path.join(base.MEDIA_ROOT, 'article')
        regex = re.compile(r'^example.*$')
        for file in os.listdir(image_dir):
            if regex.search(file):
                os.remove(os.path.join(image_dir, file))

    def test_default_visit(self):
        response = self.client.get(reverse("sth:articles"))
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "sth/article_list.html")
        articles = response.context['articles']
        self.assertEqual(10, len(articles))
        self.assertEqual(30, articles[0].num_of_comments)
        self.assertEqual(str(articles[:3]), "[<Article: Topic0>, <Article: Topic1>, <Article: Topic2>]")

    def test_all_articles(self):
        response = self.client.get(reverse("sth:articles"), {"article_category": "all"})
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "sth/article_list.html")
        articles = response.context['articles']
        self.assertEqual(10, len(articles))
        self.assertEqual(30, articles[0].num_of_comments)
        self.assertEqual(str(articles[:3]), "[<Article: Topic0>, <Article: Topic1>, <Article: Topic2>]")

    def test_sport_articles(self):
        response = self.client.get(reverse("sth:articles"), {"article_category": "sport"})
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "sth/article_list.html")
        articles = response.context['articles']
        self.assertEqual(5, len(articles))
        self.assertEqual(30, articles[0].num_of_comments)
        self.assertEqual(str(articles[:3]), "[<Article: Topic0>, <Article: Topic1>, <Article: Topic2>]")

    def test_culture_articles(self):
        response = self.client.get(reverse("sth:articles"), {"article_category": "culture"})
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "sth/article_list.html")
        articles = response.context['articles']
        self.assertEqual(5, len(articles))
        self.assertEqual(0, articles[0].num_of_comments)
        self.assertEqual(str(articles[:3]), "[<Article: Topic5>, <Article: Topic6>, <Article: Topic7>]")


class ArticleCommentsViewTest(TestCase):

    def setUp(self):
        UserFactory.reset_sequence()  # so we have the same usernames throughout tests
        ArticleFactory.reset_sequence()
        CommentFactory.reset_sequence()
        test_data()
        self.article = Article.objects.get(article_headline="Topic0")

    def tearDown(self):
        image_dir = os.path.join(base.MEDIA_ROOT, 'article')
        regex = re.compile(r'^example.*$')
        for file in os.listdir(image_dir):
            if regex.search(file):
                os.remove(os.path.join(image_dir, file))

    def test_default_visit_annonymous_date_ordered(self):
        response = self.client.get(self.article.get_absolute_url())
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "sth/article_comments.html")
        self.assertEqual(20, len(response.context["comments"]))
        self.assertFalse(response.context["my_comment"])
        self.assertContains(response, 'id="write_comment"')
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment29>, <Comment: Comment28>, <Comment: Comment27>]")

    def test_default_visit_anonymous_article_has_no_comments(self):
        self.article = Article.objects.get(article_headline="Topic1")
        response = self.client.get(self.article.get_absolute_url())
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "sth/article_comments.html")
        self.assertEqual(0, len(response.context["comments"]))
        self.assertContains(response, "No comments to show")

    def test_default_visit_logged_in_user_has_my_comment(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url())
        user = User.objects.get(id=self.client.session['_auth_user_id'])
        if logged_in:
            self.assertEqual(20, len(response.context["comments"]))
            self.assertEqual(1, len(response.context["my_comment"]))
            self.assertEqual(user.username, response.context["my_comment"][0]["username"])
            self.assertEqual("Comment0", response.context["my_comment"][0]["comment_headline"])
            self.assertNotContains(response, 'id="write_comment"')

    def test_default_visit_logged_in_user_has_no_comment(self):
        logged_in = self.client.login(username="user30", password="password1")
        response = self.client.get(self.article.get_absolute_url())
        user = User.objects.get(id=self.client.session['_auth_user_id'])
        if logged_in:
            self.assertEqual(20, len(response.context["comments"]))
            self.assertEqual(0, len(response.context["my_comment"]))
            self.assertContains(response, 'id="write_comment"')

    def test_pagination_hyperlink_all_comments_date(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get("{}?page=1&w=all&o=comment_date".format(self.article.get_absolute_url()))
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment29>, <Comment: Comment28>, <Comment: Comment27>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

        response = self.client.get("{}?page=2&w=all&o=comment_date".format(self.article.get_absolute_url()))
        comments = response.context["comments"]
        self.assertEqual(10, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment9>, <Comment: Comment8>, <Comment: Comment7>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

    def test_pagination_hyperlink_all_comments_likes(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get("{}?page=1&w=all&o=likes".format(self.article.get_absolute_url()))
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(comments[:3]),
                         "[<Comment: Comment29>, <Comment: Comment28>, <Comment: Comment27>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

        response = self.client.get("{}?page=2&w=all&o=likes".format(self.article.get_absolute_url()))
        comments = response.context["comments"]
        self.assertEqual(10, len(comments))
        self.assertEqual(str(comments[:3]),
                         "[<Comment: Comment5>, <Comment: Comment6>, <Comment: Comment7>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

    def test_pagination_hyperlink_all_ratio(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get("{}?page=1&w=all&o=ratio".format(self.article.get_absolute_url()))
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(comments[:3]),
                         "[<Comment: Comment15>, <Comment: Comment16>, <Comment: Comment17>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

        response = self.client.get("{}?page=2&w=all&o=ratio".format(self.article.get_absolute_url()))
        comments = response.context["comments"]
        self.assertEqual(10, len(comments))
        self.assertEqual(str(comments[:3]),
                         "[<Comment: Comment5>, <Comment: Comment6>, <Comment: Comment7>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

    def test_pagination_hyperlink_favourites_date(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get("{}?page=1&w=favourites&o=comment_date".format(self.article.get_absolute_url()))
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(comments[:3]),
                         "[<Comment: Comment29>, <Comment: Comment28>, <Comment: Comment27>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

        response = self.client.get("{}?page=2&w=favourites&o=comment_date".format(self.article.get_absolute_url()))
        comments = response.context["comments"]
        self.assertEqual(5, len(comments))
        self.assertEqual(str(comments[:3]),
                         "[<Comment: Comment9>, <Comment: Comment8>, <Comment: Comment7>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

    def test_pagination_hyperlink_favourites_likes(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get("{}?page=1&w=favourites&o=likes".format(self.article.get_absolute_url()))
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(comments[:3]),
                         "[<Comment: Comment29>, <Comment: Comment28>, <Comment: Comment27>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

        response = self.client.get("{}?page=2&w=favourites&o=likes".format(self.article.get_absolute_url()))
        comments = response.context["comments"]
        self.assertEqual(5, len(comments))
        self.assertEqual(str(comments[:3]),
                         "[<Comment: Comment10>, <Comment: Comment11>, <Comment: Comment12>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

    def test_pagination_hyperlink_favourites_ratio(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get("{}?page=1&w=favourites&o=ratio".format(self.article.get_absolute_url()))
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(comments[:3]),
                         "[<Comment: Comment15>, <Comment: Comment16>, <Comment: Comment17>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

        response = self.client.get("{}?page=2&w=favourites&o=ratio".format(self.article.get_absolute_url()))
        comments = response.context["comments"]
        self.assertEqual(5, len(comments))
        self.assertEqual(str(comments[:3]),
                         "[<Comment: Comment10>, <Comment: Comment11>, <Comment: Comment12>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

    def test_page_search_all_comments_date(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url(),
                                   {"page_number": 1, "w": "all", "o": "comment_date"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment29>, <Comment: Comment28>, <Comment: Comment27>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

        response = self.client.get(self.article.get_absolute_url(),
                                   {"page_number": 2, "w": "all", "o": "comment_date"})
        comments = response.context["comments"]
        self.assertEqual(10, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment9>, <Comment: Comment8>, <Comment: Comment7>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

    def test_page_search_all_comments_likes(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url(),
                                   {"page_number": 1, "w": "all", "o": "likes"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment29>, <Comment: Comment28>, <Comment: Comment27>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

        response = self.client.get(self.article.get_absolute_url(),
                                   {"page_number": 2, "w": "all", "o": "likes"})
        comments = response.context["comments"]
        self.assertEqual(10, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment5>, <Comment: Comment6>, <Comment: Comment7>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

    def test_page_search_all_comments_ratio(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url(),
                                   {"page_number": 1, "w": "all", "o": "ratio"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment15>, <Comment: Comment16>, <Comment: Comment17>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

        response = self.client.get(self.article.get_absolute_url(),
                                   {"page_number": 2, "w": "all", "o": "ratio"})
        comments = response.context["comments"]
        self.assertEqual(10, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment5>, <Comment: Comment6>, <Comment: Comment7>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

    def test_page_search_favourites_date(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url(),
                                   {"page_number": 1, "w": "favourites", "o": "comment_date"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment29>, <Comment: Comment28>, <Comment: Comment27>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

        response = self.client.get(self.article.get_absolute_url(),
                                   {"page_number": 2, "w": "favourites", "o": "comment_date"})
        comments = response.context["comments"]
        self.assertEqual(5, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment9>, <Comment: Comment8>, <Comment: Comment7>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

    def test_page_search_favourites_likes(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url(),
                                   {"page_number": 1, "w": "favourites", "o": "likes"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment29>, <Comment: Comment28>, <Comment: Comment27>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

        response = self.client.get(self.article.get_absolute_url(),
                                   {"page_number": 2, "w": "favourites", "o": "likes"})
        comments = response.context["comments"]
        self.assertEqual(5, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment10>, <Comment: Comment11>, <Comment: Comment12>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

    def test_page_search_favourites_ratio(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url(),
                                   {"page_number": 1, "w": "favourites", "o": "ratio"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment15>, <Comment: Comment16>, <Comment: Comment17>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

        response = self.client.get(self.article.get_absolute_url(),
                                   {"page_number": 2, "w": "favourites", "o": "ratio"})
        comments = response.context["comments"]
        self.assertEqual(5, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment10>, <Comment: Comment11>, <Comment: Comment12>]")
        self.assertEqual("pagination_anchor", response.context['anchor'])

    def test_criteria_form_all_comments_date(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url(),
                                   {"criteria_submit": "Search", "w": "all", "o": "comment_date"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment29>, <Comment: Comment28>, <Comment: Comment27>]")
        self.assertEqual("criteria_anchor", response.context['anchor'])

    def test_criteria_form_all_comments_likes(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url(),
                                   {"criteria_submit": "Search", "w": "all", "o": "likes"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment29>, <Comment: Comment28>, <Comment: Comment27>]")
        self.assertEqual("criteria_anchor", response.context['anchor'])

    def test_criteria_form_all_comments_ratio(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url(),
                                   {"criteria_submit": "Search", "w": "all", "o": "ratio"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment15>, <Comment: Comment16>, <Comment: Comment17>]")
        self.assertEqual("criteria_anchor", response.context['anchor'])

    def test_criteria_form_favourites_date(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url(),
                                   {"criteria_submit": "Search", "w": "favourites", "o": "date"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment29>, <Comment: Comment28>, <Comment: Comment27>]")
        self.assertEqual("criteria_anchor", response.context['anchor'])

    def test_criteria_form_favourites_likes(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url(),
                                   {"criteria_submit": "Search", "w": "favourites", "o": "likes"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment29>, <Comment: Comment28>, <Comment: Comment27>]")
        self.assertEqual("criteria_anchor", response.context['anchor'])

    def test_criteria_form_favourites_ratio(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url(),
                                   {"criteria_submit": "Search", "w": "favourites", "o": "ratio"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment15>, <Comment: Comment16>, <Comment: Comment17>]")
        self.assertEqual("criteria_anchor", response.context['anchor'])

    def test_user_search_required(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url(),
                                   {"username_search": ""})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertIn("This field is required.", response.context['user_form'].errors['username_search'])
        self.assertEqual("username_anchor", response.context['anchor'])

    def test_user_search_unsuccessful(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url(),
                                   {"username_search": "user1000000"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(0, len(comments))
        self.assertContains(response, "No comments to show")
        self.assertEqual("username_anchor", response.context['anchor'])

    def test_user_search_successful(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.get(self.article.get_absolute_url(),
                                   {"username_search": "user10"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(1, len(comments))
        self.assertEqual(comments[0].user.username, "user10")
        self.assertEqual("username_anchor", response.context['anchor'])

    def test_empty_comment_submission(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.post(self.article.get_absolute_url(),
                                    {"comment_headline": "", "content": "", "submit_comment": "Submit"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment29>, <Comment: Comment28>, <Comment: Comment27>]")
        self.assertEqual("my_comment_section", response.context['anchor'])
        self.assertIn("This field is required.",
                      response.context['write_comment_form'].errors['comment_headline'])
        self.assertIn("This field is required.",
                      response.context['write_comment_form'].errors['content'])

    def test_successful_comment_submission(self):
        logged_in = self.client.login(username="user0", password="password1")
        response = self.client.post(self.article.get_absolute_url(),
                                    {"comment_headline": "My comment", "content": "say whaaaaa\n\n hvala lepa"})
        self.assertEqual(200, response.status_code)
        comments = response.context["comments"]
        self.assertEqual(20, len(comments))
        self.assertEqual(str(response.context['comments'][:3]),
                         "[<Comment: Comment29>, <Comment: Comment28>, <Comment: Comment27>]")
        self.assertEqual(1, len(response.context["my_comment"]))
        self.assertEqual("user0", response.context["my_comment"][0]['username'])


class AjaxRatingViewTest(TestCase):
    def setUp(self):
        UserFactory.reset_sequence()  # so we have the same usernames throughout tests
        ArticleFactory.reset_sequence()
        CommentFactory.reset_sequence()
        test_data()

    def tearDown(self):
        image_dir = os.path.join(base.MEDIA_ROOT, 'article')
        regex = re.compile(r'^example.*$')
        for file in os.listdir(image_dir):
            if regex.search(file):
                os.remove(os.path.join(image_dir, file))

    def test_json_dump_plus(self):
        logged_in = self.client.login(username="user0", password="password1")
        if logged_in:
            comment = Comment.objects.get(comment_headline="Comment29")
            likes_before = comment.likes
            response = self.client.post(reverse("sth:ajaxrating"), {"rating": "+", "comment_id": comment.id})
            self.assertEqual(200, response.status_code)
            comment = Comment.objects.get(comment_headline="Comment29")
            likes_after = comment.likes
            self.assertEqual(likes_before + 1, likes_after)
            response_dict = eval(response.content)
            self.assertEqual(response_dict["server_response"], "+1")

    def test_json_dump_minus(self):
        logged_in = self.client.login(username="user0", password="password1")
        if logged_in:
            comment = Comment.objects.get(comment_headline="Comment29")
            dislikes_before = comment.dislikes
            response = self.client.post(reverse("sth:ajaxrating"), {"rating": "-", "comment_id": comment.id})
            self.assertEqual(200, response.status_code)
            comment = Comment.objects.get(comment_headline="Comment29")
            dislikes_after = comment.dislikes
            self.assertEqual(dislikes_before + 1, dislikes_after)
            response_dict = eval(response.content)
            self.assertEqual(response_dict["server_response"], "-1")

class CommentManagerTest(TestCase):

    def setUp(self):
        UserFactory.reset_sequence()  # so we have the same usernames throughout tests
        ArticleFactory.reset_sequence()
        CommentFactory.reset_sequence()
        test_data()
        self.logged_in = self.client.login(username="user0", password="password1")
        self.user = User.objects.get(id=self.client.session['_auth_user_id'])
        self.article = Article.objects.get(article_headline="Topic0")

    def tearDown(self):
        image_dir = os.path.join(base.MEDIA_ROOT, 'article')
        regex = re.compile(r'^example.*$')
        for file in os.listdir(image_dir):
            if regex.search(file):
                os.remove(os.path.join(image_dir, file))

    def test_all_comments_with_ratings_not_rated(self):
        if self.logged_in:
            comments = Comment.objects.all_comments_with_ratings(self.article, self.user)
            self.assertEqual(30, len(comments))
            for comment in comments:
                self.assertEqual(None, comment.rated)

    def test_all_comments_with_ratings_all_plus(self):
        if self.logged_in:
            prepared_comments = Comment.objects.exclude(user__username="user0").order_by('comment_headline')
            self.assertEqual(29, len(prepared_comments))
            for pc in prepared_comments:
                Rating.objects.create(user=self.user, comment=pc, rating=1)
            comments = Comment.objects.all_comments_with_ratings(self.article, self.user)
            self.assertEqual(30, len(comments))
            for comment in comments:
                if comment.user.username != "user0":
                    self.assertEqual(1, comment.rated)

    def test_all_comments_with_ratings_all_minus(self):
        if self.logged_in:
            prepared_comments = Comment.objects.exclude(user__username="user0").order_by('comment_headline')
            self.assertEqual(29, len(prepared_comments))
            for pc in prepared_comments:
                Rating.objects.create(user=self.user, comment=pc, rating=0)
            comments = Comment.objects.all_comments_with_ratings(self.article, self.user)
            self.assertEqual(30, len(comments))
            for comment in comments:
                if comment.user.username != "user0":
                    self.assertEqual(0, comment.rated)

    def test_favourite_comments_with_ratings_not_rated(self):
        if self.logged_in:
            comments = Comment.objects.favourite_comments_with_ratings(self.article, self.user)
            self.assertEqual(25, len(comments))
            for comment in comments:
                if comment.user.username != "user0":
                    self.assertEqual(None, comment.rated)

    def test_favourite_comments_with_ratings_all_plus(self):
        if self.logged_in:
            prepared_comments = Comment.objects.exclude(user__username="user0").order_by('comment_headline')
            self.assertEqual(29, len(prepared_comments))
            for pc in prepared_comments:
                Rating.objects.create(user=self.user, comment=pc, rating=1)
            comments = Comment.objects.favourite_comments_with_ratings(self.article, self.user)
            self.assertEqual(25, len(comments))
            for comment in comments:
                if comment.user.username != "user0":
                    self.assertEqual(1, comment.rated)

    def test_favourite_comments_with_ratings_all_minus(self):
        if self.logged_in:
            prepared_comments = Comment.objects.exclude(user__username="user0").order_by('comment_headline')
            self.assertEqual(29, len(prepared_comments))
            for pc in prepared_comments:
                Rating.objects.create(user=self.user, comment=pc, rating=0)
            comments = Comment.objects.favourite_comments_with_ratings(self.article, self.user)
            self.assertEqual(25, len(comments))
            for comment in comments:
                if comment.user.username != "user0":
                    self.assertEqual(0, comment.rated)

    def test_username_comment_with_ratings_not_rated(self):
        if self.logged_in:
            searched_user = User.objects.get(username="user1")
            comment_in_qs = Comment.objects.username_comment_with_ratings(self.article, self.user, searched_user)
            self.assertEqual(1, len(comment_in_qs))
            for comment in comment_in_qs:
                if comment.user.username != "user0":
                    self.assertEqual(None, comment.rated)

    def test_username_comment_with_ratings_plus(self):
        if self.logged_in:
            searched_user = User.objects.get(username="user1")
            user_comment = Comment.objects.get(user__username=searched_user.username, article=self.article)
            Rating.objects.create(user=self.user, comment=user_comment, rating=1)
            comment_in_qs = Comment.objects.username_comment_with_ratings(self.article, self.user, searched_user)
            self.assertEqual(1, len(comment_in_qs))
            for comment in comment_in_qs:
                if comment.user.username != "user0":
                    self.assertEqual(1, comment.rated)

    def test_username_comment_with_ratings_minus(self):
        if self.logged_in:
            searched_user = User.objects.get(username="user1")
            user_comment = Comment.objects.get(user__username=searched_user.username, article=self.article)
            Rating.objects.create(user=self.user, comment=user_comment, rating=0)
            comment_in_qs = Comment.objects.username_comment_with_ratings(self.article, self.user, searched_user)
            self.assertEqual(1, len(comment_in_qs))
            for comment in comment_in_qs:
                if comment.user.username != "user0":
                    self.assertEqual(0, comment.rated)
