from datetime import datetime
from django.template.defaultfilters import timesince
from django import template
from django.utils import timezone
import pytz

register = template.Library()

@register.filter
def ago(date_time):
    if date_time.tzinfo is None:
        date_time = pytz.utc.localize(date_time)
    diff = abs(date_time - timezone.now())
    if diff.days <= 0:
        span = timesince(date_time)
        span = span.split(",")[0]  # just the most significant digit
        if span == "0 minutes":
            return "seconds ago"
        return "%s ago" % span
    return datetime.date(date_time)


@register.filter
def addstr(arg1, arg2):
    """concatenate arg1 & arg2"""
    return str(arg1) + str(arg2)