from django.db import connection
from . models import *


def count_ratings():
    comments = {}
    for i in range(1, 1001):
        comments[i] = [0, 0]
    for rating in Rating.objects.all().select_related('comment'):
        if rating.rating is True:
            comments[rating.comment.id][0] += 1
        elif rating.rating is False:
            comments[rating.comment.id][1] += 1
    return comments


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]


def get_comments(user, article_id, order, which):
    ord = 'comment_date'
    if order == "likes":
        ord = 'likes DESC'
    elif order == 'ratio':
        ord = 'ratio DESC'

    cursor = connection.cursor()

    if which == "all":
        if user.is_authenticated():
            query = ("select sth_comment.id, comment_headline, content, comment_date, spam_count, "
            "article_id, sth_comment.user_id, likes, dislikes, rating, comment_id, username, "
            "(likes / greatest((likes + dislikes),1)) as ratio "
            "from sth_comment "
            "LEFT join "
            "( "
            "select rating, comment_id from sth_rating "
            "WHERE sth_rating.user_id = %s "
            ") r on r.comment_id = sth_comment.id "
            "INNER JOIN auth_user on auth_user.id = sth_comment.user_id "
            "INNER JOIN users_userprofile on users_userprofile.user_id = auth_user.id "
            "WHERE sth_comment.article_id = %s "
            "ORDER BY ")

            query += ord
            cursor.execute(query, [user.id, article_id])

        else:
            query = ("select sth_comment.id, comment_headline, content, comment_date, spam_count, "
            "article_id, sth_comment.user_id, likes, dislikes, username, "
            "(likes / greatest((likes + dislikes),1)) as ratio "
            "from sth_comment "
            "INNER JOIN auth_user on auth_user.id = sth_comment.user_id "
            "INNER JOIN users_userprofile on users_userprofile.user_id = auth_user.id "
            "WHERE sth_comment.article_id = %s "
            "ORDER BY ")

            query += ord
            cursor.execute(query, [article_id])



    #user is authenticated, because he only wants to display favourites
    else:
        query = ("select sth_comment.id, comment_headline, content, comment_date, spam_count, "
        "article_id, sth_comment.user_id, likes, dislikes, rating, comment_id, username, "
        "(likes / greatest((likes + dislikes),1)) as ratio "
        "from sth_comment "
        "LEFT join "
        "( "
        "select rating, comment_id from sth_rating "
        "WHERE sth_rating.user_id = %s "
        ") r on r.comment_id = sth_comment.id "
        "INNER JOIN auth_user on auth_user.id = sth_comment.user_id "
        "INNER JOIN users_userprofile on auth_user.id = users_userprofile.user_id "
        "INNER JOIN "
        "( "
        "select * from users_relationship "
        "WHERE users_relationship.from_person_id = %s "
        ") s on s.to_person_id = users_userprofile.id "
        "WHERE sth_comment.article_id = %s "
        "ORDER BY ")
        query += ord
        cursor.execute(query, [user.id, user.userprofile.id, article_id])

    return dictfetchall(cursor)


def get_comment(user, article_id, uname):

    cursor = connection.cursor()
    if user.is_authenticated():
        query = ("select sth_comment.id, comment_headline, content, comment_date, spam_count, "
        "article_id, user_id, likes, dislikes, rating, comment_id, username, "
        "(likes / greatest((likes + dislikes),1)) as ratio "
        "from sth_comment "
        "LEFT join "
        "( "
        "select rating, comment_id from sth_rating "
        "WHERE sth_rating.user_id = %s "
        ") r on r.comment_id = sth_comment.id "
        "INNER JOIN auth_user on auth_user.id = sth_comment.user_id "
        "WHERE sth_comment.article_id = %s AND username = %s ")

        cursor.execute(query, [user.id, article_id, uname])

    else:
        query = ("select sth_comment.id, comment_headline, content, comment_date, spam_count, "
        "article_id, user_id, likes, dislikes, username, "
        "(likes / greatest((likes + dislikes),1)) as ratio "
        "from sth_comment "
        "INNER JOIN auth_user on auth_user.id = sth_comment.user_id "
        "WHERE sth_comment.article_id = %s AND username = %s ")

        cursor.execute(query, [article_id, uname])

    return dictfetchall(cursor)


def insert_rating(user_id, comment_id, rating):
    query = ('insert into sth_rating (rating, comment_id, user_id) '
             'VALUES(%s, %s, %s)')

    cursor = connection.cursor()
    cursor.execute(query, [rating, comment_id, user_id])

    query_like = 'UPDATE sth_comment SET likes = likes + 1 WHERE id = %s'
    query_dislike = 'UPDATE sth_comment SET dislikes = dislikes + 1 WHERE id = %s'

    if rating == 1:
        cursor.execute(query_like, [comment_id])
    else:
        cursor.execute(query_dislike, [comment_id])
