import factory
import datetime
from .models import Comment
from django.utils import timezone


class CommentFactory(factory.DjangoModelFactory):
    class Meta:
        model = Comment

    article = None
    user = None
    comment_headline = factory.Sequence(lambda n: "Comment%s" % str(n))
    content = "Lorem " * 50
    comment_date = factory.Sequence(lambda n: timezone.now() - datetime.timedelta(days=n))
    likes = factory.Sequence(lambda n: (14 - n) if n < 15 else n)
    dislikes = factory.Sequence(lambda n: (29 - n) if n < 15 else (n - 15))
    spam_count = 0