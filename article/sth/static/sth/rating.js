$(document).ready(function() {
    $('.rate-popup').magnificPopup({
        items: {
            src: '#my-popup',
            type: 'inline'
        }
    });

    $(".comm").click(function(event) {
        var button_id = $(event.target).attr('id');
        var rating = button_id[0];
        var comment_id = button_id.substring(1);
        $.ajax({
            url : "/ajaxrating/",
            type : "POST",
            dataType: "json",
            data : {
                comment_id : comment_id,
                rating: rating,
                csrfmiddlewaretoken: Cookies.get('csrftoken')
            },
            success : function(json) {
                var response = json.server_response
                var div_id = '#rating_div' + comment_id
                var type = "success"
                if (response === '-1') {
                    type = "alert"
                }
                //inserts label with rating into the appropriate rating div
                $(div_id).html('<span class="' + type + ' label">' + response +'</span>');
                if (rating === "+") {
                    var likes_id = "#likes" + comment_id;
                    $(likes_id).text("+" + (parseInt($(likes_id).text().substring(1)) + 1))
                }
                else {
                    var dislikes_id = "#dislikes" + comment_id;
                    $(dislikes_id).text("-" + (parseInt($(dislikes_id).text().substring(1)) + 1))                        }

                //ratio change
                var ratio_id = "#ratio" + comment_id;
                var likes = parseInt($("#likes" + comment_id).text().substring(1))
                var dislikes = parseInt($("#dislikes" + comment_id).text().substring(1))
                $(ratio_id).text((likes / (likes + dislikes)).toFixed(2))

            },
            error : function(xhr,errmsg,err) {
                alert(xhr.status + ": " + xhr.responseText);
            }
        });
        return false;
    });
});