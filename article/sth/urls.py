from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'articles/$', views.article_list, name="articles"),
    url(r'^article/(?P<article_id>[0-9]+)/(?P<slug>[\w-]+)$', views.article_comments, name='article_comments'),
    url(r'^ajaxrating/$', views.ajaxrating, name="ajaxrating"),
]
