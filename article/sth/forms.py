from django import forms
from .models import Comment
from django.utils import timezone

comments_order = [('comment_date', 'Date'), ('likes', 'Likes'), ('ratio', 'Likes/Dislikes ratio')]
comments_which = [('all', 'All comments'), ('favourites', 'Favourite users')]
articles_which = [('all', 'All articles'), ('sport', "Sports articles"), ('culture', 'Culture articles')]

class PageForm(forms.Form):
    page_number = forms.IntegerField(min_value=1)


class CommentCriteriaForm(forms.Form):
    w = forms.ChoiceField(choices=comments_which, required=False)
    o = forms.ChoiceField(choices=comments_order)


class UserCommentSearchForm(forms.Form):
    username_search = forms.CharField()

'''
class WriteCommentForm(forms.Form):
    comment_headline = forms.CharField(max_length=70)
    content = forms.CharField(widget=forms.Textarea)
'''


class WriteCommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ("comment_headline", "content")

    def save(self, commit=True, user=None, article=None):
        comment = super(WriteCommentForm, self).save(commit=False)
        comment.likes = 0
        comment.dislikes = 0
        comment.spam_count = 0
        comment.article = article
        comment.user = user
        comment.comment_date = timezone.now()
        if commit:
            comment.save()

class ArticleCategoryForm(forms.Form):
    article_category = forms.ChoiceField(choices=articles_which)
