import json
from django.db.models import IntegerField, Case, Value, When, Count, Sum, F, Q
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.cache import cache_control
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django.utils import timezone
from django.template import RequestContext

from .models import *
from .forms import PageForm, UserCommentSearchForm, CommentCriteriaForm, WriteCommentForm, ArticleCategoryForm
from .utils import *

from django.utils.decorators import method_decorator
from django.views.generic import TemplateView


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def index(request):
    sport_articles = Article.objects.filter(category="sport").order_by("-article_date")[:4]
    culture_articles = Article.objects.filter(category="culture").order_by("-article_date")[:4]
    return render(request, 'sth/index.html', {'sport_articles': sport_articles, 'culture_articles': culture_articles})


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def article_list(request):
    category = "all"
    if request.GET.get('article_category'):
        form = ArticleCategoryForm(request.GET)
        if form.is_valid():
            category = form.cleaned_data['article_category']
    else:
        form = ArticleCategoryForm()

    if category == "all":
        articles = Article.objects.all().annotate(num_of_comments=Count("comment"))\
                                        .order_by("-article_date")
    elif category == "sport":
        articles = Article.objects.filter(category="sport")\
                                  .annotate(num_of_comments=Count("comment"))\
                                  .order_by("-article_date")
    elif category == "culture":
        articles = Article.objects.filter(category="culture")\
                                  .annotate(num_of_comments=Count("comment"))\
                                  .order_by("-article_date")
    return render(request, template_name='sth/article_list.html', context={'articles': articles, "form": form})


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def article_comments(request, article_id, slug):
    article = get_object_or_404(Article, pk=article_id)
    which = "all"
    order = "comment_date"

    # forms (4 forms)
    user_form = UserCommentSearchForm()
    # criteria_form declaration below (have to wait for which and order parameters)
    write_comment_form = WriteCommentForm()
    page_form = PageForm()

    page = 1
    my_comment = False
    all_comments = []
    username_search_result = []

    # 6 ways to get on this page (1 POST, 5 GET)

    if request.method == "POST":  # POST
        if request.POST.get('submit_comment'):  # comment was submitted (not needed after a change)
            write_comment_form = WriteCommentForm(request.POST)
            if write_comment_form.is_valid():
                comment = write_comment_form.save(user=request.user, article=article)
                messages.add_message(request, messages.INFO, "Successfully added comment")
                return redirect('{}#my_comment_section'.format(article.get_absolute_url()))
            else:
                anchor = 'my_comment_section'
    else:  # GET

        if request.GET.get('page'):  # via pagination hyperlink
            order = request.GET.get('o', "comment_date")
            which = request.GET.get('w', "all")
            page = request.GET.get('page', 1)
            anchor = 'pagination_anchor'

        elif 'page_number' in request.GET:  # form page search (parameters via hidden field)
            page_form = PageForm(request.GET)
            if page_form.is_valid():
                which = request.GET.get('w', "comment_date")
                order = request.GET.get('o', "all")
                page = page_form.cleaned_data['page_number']
            anchor = 'pagination_anchor'

        elif request.GET.get('criteria_submit'):  # criteria search results
            criteria_form = CommentCriteriaForm(request.GET)
            if criteria_form.is_valid():
                if request.user.is_authenticated():  # get[which] is empty for anon users
                    which = criteria_form.cleaned_data['w']
                order = criteria_form.cleaned_data['o']
            anchor = "criteria_anchor"

        elif 'username_search' in request.GET:  # username search results
            user_form = UserCommentSearchForm(request.GET)
            if user_form.is_valid():
                username = user_form.cleaned_data['username_search']
                if request.user.is_authenticated():
                    username_search_result = \
                        Comment.objects.username_comment_with_ratings(article, request.user, username)
                else:
                    username_search_result = Comment.objects.filter(article=article, user__username=username)
            anchor = "username_anchor"
        else:  # first visit of the page
            page = 1

    # now that we have final parameters, we can instantiate criteria form with the right parametes
    criteria_form = CommentCriteriaForm(initial={'w': which, 'o': order})
    get_arguments = "&w=" + which + "&o=" + order + ","  #this is for pagination template

    # check if user is authenticated, and if it is has he already written a comment in this article
    if request.user.is_authenticated():
        my_comment = get_comment(request.user, article.id, request.user.username)  #empty list will evaluate to False

    # WHICH COMMENTS TO SHOW
    if user_form.is_valid():
        all_comments = username_search_result
    else:
        if which == "all":
            if request.user.is_authenticated():
                all_comments_queryset = Comment.objects.all_comments_with_ratings(article, request.user)
            else:
                all_comments_queryset = Comment.objects.filter(article=article)

        else:  # means that user is already logged in because he only wants comments from his favourite users
            all_comments_queryset = Comment.objects.favourite_comments_with_ratings(article, request.user)

        #now that we have the required comments we will order them
        if order == "ratio":
            all_comments = sorted(all_comments_queryset, key=lambda c: c.ratio, reverse=True)
        else:
            final_order = "comment_date" if order == "comment_date" else "-likes"
            all_comments = all_comments_queryset.order_by(final_order)

    # if len(all_comments) > 1:
    paginator = Paginator(all_comments, 20)
    try:
        comments = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        comments = paginator.page(1)
        page = 1
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        comments = paginator.page(paginator.num_pages)

    context={'article': article, 'criteria_form': criteria_form, 'user_form': user_form,
            'page_form': page_form, 'write_comment_form': write_comment_form,
            'comments': comments, 'which': which, 'order': order,
            'my_comment': my_comment,  # empty or not
            'get_arguments': get_arguments
            }
    # if validation failed on write_comment_form, then we want to be thrown to the form in the template
    try:
        anchor
    except NameError:
        anchor = False

    context['anchor'] = anchor

    return render(request, template_name='sth/article_comments.html', context=context)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def ajaxrating(request):
    if "rating" in request.POST:
        response_dict = {}
        if request.POST.get('rating') == '+':
            rating = 1
        else:
            rating = 0
        comment = Comment.objects.get(pk=request.POST.get('comment_id'))
        new_rating = Rating.objects.create(user=request.user, comment=comment, rating=int(rating))
        if rating == 1:
            comment.likes += 1
        else:
            comment.dislikes += 1
        comment.save()

        rating_string = '+1' if rating == 1 else '-1'
        response_dict.update({'server_response': rating_string})
        return HttpResponse(json.dumps(response_dict), content_type='application/javascript')
    else:
        return render(request, 'sth/article_comments.html', context_instance=RequestContext(request))
