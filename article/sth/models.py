from django.db import models
from django.contrib.auth.models import User
from django.db.models import Case, When, IntegerField, F, Sum
from django.utils.text import slugify
from django.core.urlresolvers import reverse

CATEGORY_CHOICES = (
    ('sport', "Sport"),
    ('culture', "Culture")
)


class CommentManager(models.Manager):
    def all_comments_with_ratings(self, article, user):
        return Comment.objects.filter(article=article) \
                              .annotate(rated=Sum(Case(When(rating__user=user, then=F('rating__rating')),
                                                            output_field=IntegerField()))) \
                              .select_related('user')


    def favourite_comments_with_ratings(self, article, user):
        return Comment.objects.filter(article=article, user__userprofile__to_people__from_person=user.userprofile) \
                              .annotate(rated=Sum(Case(When(rating__user=user, then=F('rating__rating')),
                                                          output_field=IntegerField()
                                                      )
                                                 )
                                       ) \
                              .select_related('user')

    def username_comment_with_ratings(self, article, user, searched_user):
        return Comment.objects.filter(article=article, user__username=searched_user) \
                              .annotate(rated=Sum(Case(When(rating__user=user, then=F('rating__rating')),
                                                            output_field=IntegerField()
                                                      )
                                                 )
                                       ) \
                              .select_related('user')


class Article(models.Model):
    article_headline = models.CharField(max_length=100)
    article_date = models.DateTimeField()
    image = models.ImageField(upload_to="article/")
    description = models.TextField(default="")
    content = models.TextField(default="")
    category = models.CharField(choices=CATEGORY_CHOICES, max_length=20, default="sport")
    slug = models.SlugField(max_length=80, default="bar")

    def save(self, *args, **kwargs):
        self.slug = slugify(self.article_headline)
        super(Article, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("sth:article_comments", kwargs={"article_id": self.id, "slug": self.slug})

    def num_of_comments(self):
        return self.comment_set.count()

    def __str__(self):
        return self.article_headline


class Comment(models.Model):
    objects = CommentManager()

    comment_headline = models.CharField(max_length=70)
    article = models.ForeignKey(Article, db_index=True)
    user = models.ForeignKey(User, db_index=True)
    content = models.TextField(default="", max_length=12000)
    comment_date = models.DateTimeField()
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    spam_count = models.IntegerField(default=0)

    def _ratio(self):
        all_ratings = self.likes + self.dislikes
        if all_ratings > 0:
            return format(self.likes / all_ratings, ".2f")
        else:
            return "0.00"

    ratio = property(_ratio)

    def __str__(self):
        return self.comment_headline


class Rating(models.Model):
    user = models.ForeignKey(User, db_index=True)
    comment = models.ForeignKey(Comment, db_index=True)
    rating = models.BooleanField()

    def __str__(self):
        return self.user.username + " rated " + self.comment.comment_headline + " " + str(self.rating)

