from .base import *

ALLOWED_HOSTS = ['*']

DEBUG = False
'''
if "COMPANY_IS_ON_AWS" in os.environ:
    DEBUG = False
else:
    DEBUG = True
'''

if 'RDS_HOSTNAME' in os.environ:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': os.environ['RDS_DB_NAME'],
            'USER': os.environ['RDS_USERNAME'],
            'PASSWORD': os.environ['RDS_PASSWORD'],
            'HOST': os.environ['RDS_HOSTNAME'],
            'PORT': os.environ['RDS_PORT'],
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'my_app',
            'USER': 'my_app',
            'PASSWORD': '123',
            'HOST' : 'localhost',
            'PORT' : '',
        }
    }

# email
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = os.environ.get("LOCAL_EMAIL_HOST_USER", '')
EMAIL_HOST_PASSWORD = os.environ.get("LOCAL_EMAIL_HOST_PASSWORD", '')

DEFAULT_FROM_EMAIL = EMAIL_HOST_USER  # where user feedback is sent from
SERVER_EMAIL = EMAIL_HOST_USER  # where exception notives are sent from

MANAGERS = [('Jest', 'bananc12345@gmail.com')]

STATIC_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, STATICFILES_LOCATION)
STATICFILES_STORAGE = 'custom_storages.StaticStorage'
MEDIA_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)
DEFAULT_FILE_STORAGE = 'custom_storages.MediaStorage'

# HTTPS
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_SSL_REDIRECT = True
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
