from .base import *

DEBUG = True
#ALLOWED_HOSTS = ['localhost', '127.0.0.1']

INSTALLED_APPS += (
    'djangobower',
)

local_static_testing = True
local_database = True


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

if local_database:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'articledb',
            'USER': os.environ.get("LOCAL_DB_USER", ''),
            'PASSWORD': os.environ.get("LOCAL_DB_PASSWORD", ''),
            'HOST': 'localhost',   # Or an IP Address that your DB is hosted on
            'PORT': '3306',
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': os.environ['RDS_DB_NAME'],
            'USER': os.environ['RDS_USERNAME'],
            'PASSWORD': os.environ['RDS_PASSWORD'],
            'HOST': os.environ['RDS_HOSTNAME'],
            'PORT': os.environ['RDS_PORT'],
        }
    }



if local_static_testing:
    STATIC_URL = "/static/"
    MEDIA_URL = '/media/'
else:
    STATIC_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, STATICFILES_LOCATION)
    # Tell the staticfiles app to use S3Boto storage when writing the collected static files (when
    # you run `collectstatic`).
    STATICFILES_STORAGE = 'custom_storages.StaticStorage'
    MEDIA_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)
    DEFAULT_FILE_STORAGE = 'custom_storages.MediaStorage'

#static - only for local stuff
STATICFILES_FINDERS += (
    'djangobower.finders.BowerFinder',
    'compressor.finders.CompressorFinder',
)

BOWER_COMPONENTS_ROOT = os.path.join(BASE_DIR, 'components')  # for BowerFinder

BOWER_INSTALLED_APPS = (
    'foundation-sites',
    'magnific-popup',
    'js-cookie'
)

#STATIC_URL = '/static/'


# django-compressor
COMPRESS_ENABLED = False

COMPRESS_ROOT = STATIC_ROOT  # where files are going to be compressed to
COMPRESS_URL = STATIC_URL

COMPRESS_PRECOMPILERS = (
        ('text/x-sass', 'sass --compass --style compressed "{infile}" "{outfile}"'),
        ('text/x-scss', 'sass --scss --compass --style compressed -I "%s/bower_components" "{infile}" "{outfile}"' % BOWER_COMPONENTS_ROOT),
    )

# email
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = os.environ.get("LOCAL_EMAIL_HOST_USER", '')
EMAIL_HOST_PASSWORD = os.environ.get("LOCAL_EMAIL_HOST_PASSWORD", '')

DEFAULT_FROM_EMAIL = EMAIL_HOST_USER  # where user feedback is sent from
SERVER_EMAIL = EMAIL_HOST_USER  # where exception notices are sent from

MANAGERS = [('Jest', 'bananc12345@gmail.com')]


