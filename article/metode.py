def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]


def que():
    q = ("select username, num_of_likes, num_of_dislikes, "
    "(num_of_likes / GREATEST((num_of_dislikes + num_od_dislikes), 1)) as ratio "
    "from ( "
    "select username, sum(likes) as num_of_likes, sum(dislikes) as num_of_dislikes "
    "from sth_comment inner join auth_user on sth_comment.user_id = auth_user.id "
    "where username = user0 "
    ") r ")
    return q

