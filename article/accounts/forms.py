from django import forms
from registration.forms import RegistrationFormUniqueEmail
from django_countries import countries
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth.password_validation import NumericPasswordValidator, MinimumLengthValidator
from settings.base import AUTH_PASSWORD_VALIDATORS
from django.core.exceptions import ImproperlyConfigured, ValidationError


COUNTRY_CHOICES = [('', 'Select country')] + list(countries)


def validate_minimal_length(password):
    min_length = -1
    for validator in AUTH_PASSWORD_VALIDATORS:
        if validator["NAME"] == "django.contrib.auth.password_validation.MinimumLengthValidator":
            min_length = validator["OPTIONS"]['min_length']
    if 0 < len(password) < min_length:
        raise ValidationError(
                _("This password is too short. It must contain at least %(value)s characters."),
                code='password_too_short', params={'value': min_length}
            )


def validate_numeric_password(password):
    if password.isdigit():
        raise ValidationError(
            _("This password is entirely numeric."),
            code='password_entirely_numeric',
        )


class UserProfileRegistrationForm(RegistrationFormUniqueEmail):
    password1 = forms.CharField(label=_("Password"),
        widget=forms.PasswordInput,
        validators=[validate_numeric_password, validate_minimal_length])

    country = forms.ChoiceField(choices=COUNTRY_CHOICES)  # for inserting user_profile
