from django.conf.urls import include, url
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import views as auth_views
from django.views.generic.base import TemplateView

from . import views  # ???
from .decorators import anonymous_required

from .views import MyRegistrationView, MyActivationView

urlpatterns = [
    url(r'^login/$', auth_views.login, {'template_name': 'accounts/login.html'}, name="login"),
    url(r'^logout/$', auth_views.logout, {'template_name': 'accounts/logout.html', 'next_page': 'sth:index'},
        name="logout"),
    url(r'^reset/$', auth_views.password_reset, {'template_name': 'accounts/reset.html',
                                                 "email_template_name": 'accounts/reset_email.html',
                                                 "subject_template_name": 'accounts/reset_subject.txt',
                                                 "post_reset_redirect": reverse_lazy('accounts:reset_done')},
        name='reset'),
    url(r'^password/reset/done/$', auth_views.password_reset_done, {'template_name': 'accounts/reset_done.html'},
        name='reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
        auth_views.password_reset_confirm, {"template_name": 'accounts/reset_confirm.html',
                                            "post_reset_redirect": reverse_lazy('accounts:login')},
        name='reset_confirm'),
]


registrationpatterns = [
                       url(r'^activate/complete/$',
                           TemplateView.as_view(template_name='registration/activation_complete.html'),
                           name='registration_activation_complete'),
                       # Activation keys get matched by \w+ instead of the more specific
                       # [a-fA-F0-9]{40} because a bad activation key should still get to the view;
                       # that way it can return a sensible "invalid key" message instead of a
                       # confusing 404.
                       url(r'^activate/(?P<activation_key>\w+)/$',
                           MyActivationView.as_view(),
                           name='registration_activate'),
                       url(r'^register/complete/$',
                           TemplateView.as_view(template_name='registration/registration_complete.html'),
                           name='registration_complete'),
                       url(r'^register/closed/$',
                           TemplateView.as_view(template_name='registration/registration_closed.html'),
                           name='registration_disallowed'),

                       url(r'^register/$', MyRegistrationView.as_view(), name='registration_register'),
                       url(r'^checkavailability/$', views.checkavailability),
                       ]

urlpatterns += registrationpatterns