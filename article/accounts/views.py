import re
import json

from django.forms import ValidationError
from django.contrib.auth.models import User
from django.core.validators import validate_email
from django.http import HttpResponse
from django.shortcuts import render
from django.template import RequestContext
from django.views.decorators.cache import cache_control
from django.utils.decorators import method_decorator

from .decorators import anonymous_required
from .forms import UserProfileRegistrationForm
from users.models import UserProfile
from registration.backends.default.views import RegistrationView, ActivationView
from registration.forms import RegistrationFormUniqueEmail


def checkavailability(request):  # check for availability and also some email validation
    # is_present: did user enter any value
    # email:
    response_dict = {}
    if "field_id" in request.POST:
        if request.POST.get('field_id') == 'id_email':  # check which field are we checking
            response_dict.update({'email_free': 1, "is_valid": 1, "value_present": 1})  # 1 ok
            email = request.POST.get("user_input")
            if len(email) == 0:
                response_dict.update({"value_present": 0})
            email_search = User.objects.filter(email__iexact=email)
            if len(email_search) != 0:
                response_dict.update({'email_free': 0})  # 0 not ok

            try:  # here we also do email validation so we do not have to type the same validator in javascript
                validate_email(email)
            except ValidationError:
                response_dict.update({"is_valid": 0})
        elif request.POST.get('field_id') == 'id_username':  # here we are checking username
            response_dict.update({'username_free': 1, "is_valid": 1, "value_present": 1})
            username = request.POST.get("user_input")
            if len(username) == 0:
                response_dict.update({"value_present": 0})
            username_search = User.objects.filter(username__iexact=username)
            if len(username_search) != 0:
                response_dict.update({'username_free': 0})

            if not re.match(r'^[\w.@+-]+$', username):  # check for the correct characters
                response_dict.update({"is_valid": 0})
        return HttpResponse(json.dumps(response_dict), content_type='application/javascript')
    else:
        return render(request, 'accounts/registration_form.html', context_instance=RequestContext(request))


@method_decorator(anonymous_required, name='dispatch')
@method_decorator(cache_control(no_cache=True, must_revalidate=True, no_store=True), name='dispatch')
class MyRegistrationView(RegistrationView):
    template_name = 'accounts/registration_form.html'
    success_url = "accounts:registration_complete"
    form_class = UserProfileRegistrationForm

    def register(self, form_class):
        new_user = super(MyRegistrationView, self).register(form_class)
        user_profile = UserProfile(user=new_user, country=form_class.cleaned_data['country'])
        user_profile.save()  # can be changed to new_user.save() if we create post_save signal on user
        return new_user


@method_decorator(cache_control(no_cache=True, must_revalidate=True, no_store=True), name='dispatch')
class MyActivationView(ActivationView):
    #failed activation template is registration/activate.html, which is also a template_name value in parent class
    def get_success_url(self, user):
        return ('accounts:registration_activation_complete', (), {})