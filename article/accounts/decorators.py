from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse


def anonymous_required(function):

    def new_func(request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse("sth:index"))
        else:
            return function(request, *args, **kwargs)
    return new_func
