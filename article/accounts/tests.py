import os

from django.test import TestCase, RequestFactory
from django.core.urlresolvers import reverse
from django.core import mail
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm
from django.template.loader import render_to_string
from django.contrib.sites.models import Site

from settings.base import BASE_DIR
from registration.models import RegistrationProfile
from users.factories import UserFactory
from .forms import UserProfileRegistrationForm


def test_data():
    users = []
    for i in range(10):
        users.append(UserFactory())


class LoginViewTest(TestCase):

    def setUp(self):
        UserFactory.reset_sequence()
        test_data()

    def test_successful_login(self):
        response = self.client.post(reverse("accounts:login"),
                                    {"username": "user0", "password": "password1"},
                                    follow=True)
        self.assertIn('_auth_user_id', self.client.session.keys())
        self.assertRedirects(response, reverse("sth:index"))
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'sth/index.html')

    def unsuccessful_login(self):
        response = self.client.post(reverse("accounts:login"),
                                    {"username": "user0", "password": "wrongpassword"},
                                    follow=True)

        self.assertNotIn('_auth_user_id', self.client.session.keys())
        self.assertEqual(200, response.status_code)
        self.assertRedirects(response, reverse("sth:login"))
        self.assertTemplateUsed(response, 'accounts/login.html')


class AuthenticationFormTest(TestCase):
    def setUp(self):
        UserFactory.reset_sequence()
        test_data()

    def test_empty_form(self):
        data = {"username": "", "password": ""}
        form = AuthenticationForm(request=RequestFactory.request, data=data)
        self.assertIn("This field is required.", form.errors["username"])
        self.assertIn("This field is required.", form.errors["password"])

    def test_the_wrong_username(self):
        data = {"username": "wrongusername", "password": "nekineki"}
        form = AuthenticationForm(request=RequestFactory().request, data=data)
        self.assertIn("Please enter a correct username and password. Note that both fields may be case-sensitive.",
                      form.non_field_errors())


class PasswordResetViewTest(TestCase):

    def setUp(self):
        UserFactory.reset_sequence()
        test_data()

    def test_unsuccessful_submission(self):
        data = {"email": ""}
        response = self.client.post(reverse("accounts:reset"), data=data)
        self.assertEqual(200, response.status_code)
        self.assertIn("This field is required.", response.context["form"].errors['email'])
        self.assertTemplateUsed(response, "accounts/reset.html")

    def test_successful_email_submission(self):
        user_email = 'user0@email.com'
        data = {"email": user_email}
        response = self.client.post(reverse("accounts:reset"), data=data, follow=True)
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "accounts/reset_done.html")
        self.assertRedirects(response, reverse("accounts:reset_done"))
        self.assertEqual(1, len(mail.outbox))
        email = mail.outbox[0]
        self.assertIn(user_email, email.recipients())


class PasswordResetFormTest(TestCase):

    def setUp(self):
        UserFactory.reset_sequence()
        test_data()

    def test_empty_form(self):
        data = {"email": ''}
        form = PasswordResetForm(data=data)
        self.assertIn('This field is required.', form.errors['email'])

    def test_wrong_email_format(self):
        data = {"email": 'wrongformat'}
        form = PasswordResetForm(data=data)
        self.assertIn('Enter a valid email address.', form.errors['email'])

    def test_correct_email(self):
        email_address = "user0@email.com"
        data = {"email": email_address}
        form = PasswordResetForm(data=data)
        if form.is_valid():
            request = RequestFactory().request
            form.save(subject_template_name='accounts/reset_subject.txt',
                      email_template_name='accounts/reset_email.html',
                      request=request)
            message = mail.outbox[0]
            self.assertEqual(1, len(mail.outbox))
            self.assertEqual(message.recipients(), [u.email for u in form.get_users(email_address)])



class RegistrationViewTest(TestCase):

    def setUp(self):
        UserFactory.reset_sequence()
        test_data()
        site = Site.objects.get(id=1)
        site.name = "newssite.com"
        site.save()

    def test_unsuccessful_form_submission(self):
        data = {"username": '',
                "email": '',
                "password1": '',
                "password2": '',
                "country": ''}

        response = self.client.post(reverse("accounts:registration_register"), data=data, follow=True)
        self.assertEqual(200, response.status_code)
        form = response.context["form"]
        self.assertIn('This field is required.', form.errors['username'])
        self.assertIn('This field is required.', form.errors['email'])
        self.assertIn('This field is required.', form.errors['password1'])
        self.assertIn('This field is required.', form.errors['password2'])
        self.assertIn('This field is required.', form.errors['country'])
        self.assertEqual(5, len(form.errors))

    def test_successful_form_submission(self):
        data = {"username": 'testuser',
                "email": 'testuser@email.com',
                "password1": 'password1',
                "password2": 'password1',
                "country": 'SI'}

        response = self.client.post(reverse("accounts:registration_register"), data=data, follow=True)
        self.assertEqual(200, response.status_code)
        self.assertRedirects(response, reverse("accounts:registration_complete"))
        message = mail.outbox[0]
        self.assertEqual(1, len(mail.outbox))
        with open(os.path.join(BASE_DIR,
                               "accounts/templates/accounts/activation_email_subject.txt"), "r") as f:
            site = Site.objects.get(id=1)
            ctx = {"site": site}
            file_subject = render_to_string('accounts/activation_email_subject.txt', ctx)
            self.assertEqual(message.subject, file_subject)


class RegistrationFormTest(TestCase):

    def setUp(self):
        UserFactory.reset_sequence()
        test_data()

    def test_empty_form(self):
        data = {"username": '',
                "email": '',
                "password1": '',
                "password2": '',
                "country": ''}
        form = UserProfileRegistrationForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertIn('This field is required.', form.errors['username'])
        self.assertIn('This field is required.', form.errors['email'])
        self.assertIn('This field is required.', form.errors['password1'])
        self.assertIn('This field is required.', form.errors['password2'])
        self.assertIn('This field is required.', form.errors['country'])

    def test_wrong_username_and_email_format(self):
        data = {"username": 'user!',
                "email": 'nekibrezafne',
                "password1": '',
                "password2": '',
                "country": ''}
        form = UserProfileRegistrationForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertIn('Enter a valid username. This value may contain only ' +
                      'letters, numbers and @/./+/-/_ characters.',
                      form.errors['username'])
        self.assertIn('Enter a valid email address.', form.errors['email'])

    def test_already_used_username_and_email(self):
        data = {"username": 'user0',
                "email": 'user1@email.com',
                "password1": '',
                "password2": '',
                "country": ''}
        form = UserProfileRegistrationForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertIn('A user with that username already exists.', form.errors['username'])
        self.assertIn('This email address is already in use. Please supply a different email address.',
                      form.errors['email'])

    def test_password_to_short(self):
        data = {"username": 'user!',
                "email": 'nekibrezafne',
                "password1": 'four',
                "password2": 'four',
                "country": ''}
        form = UserProfileRegistrationForm(data=data)
        self.assertFalse(form.is_valid())

        self.assertIn('This password is too short. It must contain at least 6 characters.',
                      form.errors['password1'])

    def test_password_numeric(self):
        data = {"username": 'user!',
                "email": 'nekibrezafne',
                "password1": '12345678',
                "password2": '12345678',
                "country": ''}
        form = UserProfileRegistrationForm(data=data)
        self.assertFalse(form.is_valid())

        self.assertIn('This password is entirely numeric.',
                      form.errors['password1'])

    def test_passwords_not_matching(self):
        data = {"username": 'user!',
                "email": 'nekibrezafne',
                "password1": 'password1',
                "password2": 'password2',
                "country": ''}
        form = UserProfileRegistrationForm(data=data)
        self.assertFalse(form.is_valid())

        self.assertIn("The two password fields didn't match.",
                      form.errors['password2'])


class RegistrationViewTest(TestCase):

    def setUp(self):
        UserFactory.reset_sequence()
        test_data()
        site = Site.objects.get(id=1)
        site.name = "newssite.com"
        site.save()

    def test_successful_activation(self):
        data = {"username": 'testuser',
                "email": 'testuser@email.com',
                "password1": 'password1',
                "password2": 'password1',
                "country": 'SI'}

        response = self.client.post(reverse("accounts:registration_register"), data=data, follow=True)
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "registration/registration_complete.html")
        message = mail.outbox[0]
        self.assertEqual(1, len(mail.outbox))
        profile = RegistrationProfile.objects.get(user__email=message.recipients()[0])
        self.assertEqual(0, profile.activated)
        activation_key = profile.activation_key
        self.assertIn(activation_key, message.body)

        kwargs = {"activation_key": activation_key}
        activation_response = self.client.get(reverse("accounts:registration_activate", kwargs=kwargs), follow=True)
        self.assertTemplateUsed(activation_response, "registration/activation_complete.html")
        profile = RegistrationProfile.objects.get(user__email=message.recipients()[0])
        self.assertEqual(1, profile.activated)
        logged_in = self.client.login(username="testuser", password="password1")
        self.assertTrue(logged_in)
        self.assertIn("_auth_user_id", self.client.session)

    def test_unsuccessful_activation(self):
        kwargs = {"activation_key": "wronkkey"}
        activation_response = self.client.get(reverse("accounts:registration_activate", kwargs=kwargs), follow=True)
        self.assertTemplateUsed(activation_response, "registration/activate.html")
        self.assertEqual(200, activation_response.status_code)
