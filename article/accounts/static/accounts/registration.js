$(document).ready(function() {
    //submit form if no errors are found (check for class .error-input) //check first if every field has input!!!
    $("form").submit(function(event) {
        if (errorsFound()) {
            event.preventDefault();
        }
    });

    // country validation (client side)
    $("#id_country").focusout(function(event) {
        var user_input = $(event.target).val();
        if (user_input.length == 0) {
            addErrorStyle("#id_country", "#client_country", "This field is required.");
        }
        else {
            removeErrorStyle("#id_country", "#client_country");
        }
    });

    //password1 validation (client side) - length and numerical password prevention
    $("#id_password1").focusout(function(event) {
        var user_input = $(event.target).val();
        //did user put anything into input
        if (user_input.length == 0) {
            addErrorStyle("#id_password1", "#client_password1", "This field is required.");
        }
        //password length validation
        else if (user_input.length < 6) {
            addErrorStyle("#id_password1", "#client_password1", "This password is too short. It must contain at least 6 characters.");
        }
        //numerical password prevention
        else if (isNumeric(user_input)) {
            addErrorStyle("#id_password1", "#client_password1", "This password is entirely numeric.");
        }
        //valid password
        else {
            removeErrorStyle("#id_password1", "#client_password1");
            var password2 = $("#id_password2").val();
            if (password2.length > 0 && password2 != user_input) {
                addErrorStyle("#id_password2", "#client_password2", "Passwords do not match");
            }
            else {
                removeErrorStyle("#id_password2", "#client_password2");
            }
        }
    });

    //password2 validation (only check if it matches password1)
    $("#id_password2").focusout(function(event) {
        var user_input = $(event.target).val();
        var password1 = $("#id_password1").val();
        //did user put anything into input
        if (user_input.length == 0) {
            addErrorStyle("#id_password2", "#client_password2", "This field is required.");
        }
        //does it match password1
        else if (user_input !== password1) {
            addErrorStyle("#id_password2", "#client_password2", "Passwords do not match");
        }
        //valid password
        else {
            removeErrorStyle("#id_password2", "#client_password2");
            var password1 = $("#id_password2").val();
            if (password1.length > 0 && password1 != user_input) {
                addErrorStyle("#id_password1", "#client_password1", "Passwords do not match");
            }
        }
    });

    //username and email validation (client side)
    if ( $('#registration_form').length ) {
        $("#id_username, #id_email").focusout(function (event) {
            var field_id = $(event.target).attr('id');
            var user_input = $("#" + field_id).val();
            $.ajax({
                url: "/accounts/checkavailability/", //need to pass the whole url
                type: "POST",
                dataType: "json",
                data: {
                    field_id: field_id,
                    user_input: user_input,
                    csrfmiddlewaretoken: Cookies.get('csrftoken')
                },
                success: function (json) {
                    //email "checkers" not validators
                    if (json.hasOwnProperty("email_free")) {
                        //did user put anything into input?
                        if (json.value_present == 0) {
                            addErrorStyle("#id_email", "#client_email", "This field is required.");
                        }
                        //is email still available?
                        else if (json.email_free == 0) {
                            addErrorStyle("#id_email", "#client_email", "A user with that email already exists.");
                        }
                        //is email address valid?
                        else if (json.is_valid == 0) {
                            addErrorStyle("#id_email", "#client_email", "Please enter a valid email address.");
                        }
                        //valid email address
                        else {
                            removeErrorStyle("#id_email", "#client_email");
                        }
                    }
                    //username
                    else {
                        //did user put anything into input?
                        if (json.value_present == 0) {
                            addErrorStyle("#id_username", "#client_username", "This field is required.");
                        }
                        //is username still available?
                        else if (json.username_free == 0) {
                            addErrorStyle("#id_username", "#client_username", "A user with that username already exists.");
                        }
                        //does username contain valid characters
                        else if (json.is_valid == 0) {
                            addErrorStyle("#id_username", "#client_username", "Enter a valid username. This value may contain may contain only letters, numbers and next five characters: @ . + - _ ");
                        }
                        //valid username
                        else {
                            removeErrorStyle("#id_username", "#client_username");
                        }
                    }
                },
                error: function (xhr, errmsg, err) {
                    alert(xhr.status + ": " + xhr.responseText);
                }
            });
            return false;
        });
    }
});

function addErrorStyle(id_field, id_errorlabel, error_text) {
    $(id_field).addClass("error-input");
    $(id_errorlabel).removeClass("ajaxcheck");
    $(id_errorlabel).text(error_text);
}

function removeErrorStyle(id_field, id_errorlabel) {
    $(id_field).removeClass("error-input");
    $(id_errorlabel).addClass("ajaxcheck");
}

function isNumeric(value) {
    return /^\d+$/.test(value);
}

function errorsFound() {
    var found = false, field_id;
    $(".thefield").each(function() {
        if ($(this).val().length == 0) {
            field_id = $(this).attr("id");
            addErrorStyle(field_id, "#client_" + field_id.substring(3), "This field is required.");
            found = true;
        }
    });
    $(".thefield").each(function() {
        if ($(this).hasClass("error-input")) {
            found = true;
        }
    });
    return found;
}
