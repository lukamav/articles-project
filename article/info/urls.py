from django.conf.urls import url

from .views import *
from . import views

urlpatterns = [
    url(r'^info/$', TemplateView.as_view(template_name="info/info.html"), name="info"),
]

contacturls = [
    url(r'^$',
        MyContactFormView.as_view(),
        name='contact_form'),
    url(r'^sent/$',
        TemplateView.as_view(
            template_name='contact_form/contact_form_sent.html'),
        name='contact_form_sent'),
]

urlpatterns += contacturls
