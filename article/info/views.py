from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, TemplateView, ListView
from django.views.decorators.cache import cache_control

from .models import *
from contact_form.views import ContactFormView
from contact_form.forms import ContactForm


class MyContactFormView(ContactFormView):

    def get_success_url(self):
        # This is in a method instead of the success_url attribute
        # because doing it as an attribute would involve a
        # module-level call to reverse(), creating a circular
        # dependency between the URLConf (which imports this module)
        # and this module (which would need to access the URLConf to
        # make the reverse() call).
        return reverse('info:contact_form_sent')
