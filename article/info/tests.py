from django.test import TestCase, RequestFactory
from django.core.urlresolvers import reverse
from django.core import mail
from contact_form.forms import ContactForm
from settings import local
# Create your tests here.

class ContactFormTest(TestCase):

    def test_get(self):
        """
        HTTP GET on the form view just shows the form.
        Copied from contact_form package just to test if the right template is loaded
        """
        contact_url = reverse('info:contact_form')

        response = self.client.get(contact_url)
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response,
                                'contact_form/contact_form.html')

    def test_successful_form_submission(self):
        """
        Valid data through the view results in a successful send.
        Copied from contact_form package just to test if the right template is loaded
        """
        contact_url = reverse('info:contact_form')
        data = {'name': 'Test',
                'email': 'test@example.com',
                'body': 'Test message'}

        response = self.client.post(contact_url,
                                    data=data, follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response,
                             reverse('info:contact_form_sent'))
        self.assertTemplateUsed(response,
                                'contact_form/contact_form_sent.html')
        self.assertEqual(1, len(mail.outbox))

        message = mail.outbox[0]
        self.assertTrue(data['body'] in message.body)
        self.assertEqual(local.DEFAULT_FROM_EMAIL,
                         message.from_email)
        form = ContactForm(request=RequestFactory().request)
        self.assertEqual(form.recipient_list,
                         message.recipients())