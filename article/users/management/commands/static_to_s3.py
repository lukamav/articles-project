from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
import os
import boto3


class Command(BaseCommand):

    def handle(self, *args, **options):
        bucket_name = 'jest-article-bucket'
        css_file_name = 'app.a187ab7ccc61.css'
        js_file_name = 'o4d6ca65ef46.js'

        s3 = boto3.resource('s3')
        #empty the old static files
        bucket = s3.Bucket(bucket_name)
        for obj in bucket.objects.filter(Prefix='static/dist/'):
            s3.Object(bucket.name, obj.key).delete()
        # add the new static files
        s3.Object(bucket_name, 'static/dist/css/' + css_file_name).\
            put(Body=open('static/dist/css/' + css_file_name, 'rb'), ContentType="text/css")  # all css
        s3.Object(bucket_name, 'static/dist/js/f02bcb9c2b0f.js').\
            put(Body=open('static/dist/js/f02bcb9c2b0f.js', 'rb'), ContentType="application/javascript")  # foundation
        s3.Object(bucket_name, 'static/dist/js/' + js_file_name).\
            put(Body=open('static/dist/js/' + js_file_name, 'rb'), ContentType="application/javascript")  # other js

