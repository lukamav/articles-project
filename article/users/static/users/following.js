$(document).ready(function() {
    if ($("#followed").val() == 1) {
        $('input[id^="1"]').hide();
    }
    else {
        $('input[id^="0"]').hide();
    }
    $(".foll").click(function(event) {
        var button_id = $(event.target).attr('id');
        var type = button_id[0];
        var user_id = button_id.substring(1);
        $.ajax({
            url : "/users/ajaxfollowing/",
            type : "POST",
            dataType: "json",
            data : {
                type : type,
                user_id: user_id,
                csrfmiddlewaretoken: Cookies.get('csrftoken')
            },
            success : function(json) {
                if (type === "1") {
                    $("#0" + user_id).show()
                    $("#1" + user_id).hide()
                }
                else {
                    $("#1" + user_id).show()
                    $("#0" + user_id).hide()
                }
            },
            error : function(xhr,errmsg,err) {
                alert(xhr.status + ": " + xhr.responseText);
            }
        });
        return false;
    });
});