import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.views import password_change
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.views.decorators.cache import cache_control
from django.views.generic import ListView
from django.utils.decorators import method_decorator

from .models import *
from .utils import one_user_likes, one_user_comments, insert_following, delete_following
from .forms import UserSettingsForm


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def user_details(request, username):
    up = get_object_or_404(UserProfile, user__username=username)
    user_likes = one_user_likes(username)
    is_followed = 0
    if request.user.is_authenticated() and request.user.is_staff is False:
        try:
            check_rel = Relationship.objects.get(from_person_id=request.user.userprofile.id,
                                                    to_person_id=up.id, rel_status=1)
        except Relationship.DoesNotExist:
            check_rel = None

        if check_rel is not None:
            is_followed = 1
    return render(request, 'users/user_details.html', {'user_profile': up, 'is_followed': is_followed,
                                                    'user_likes': user_likes})


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required
def settings_view(request):
    u = request.user
    up = u.userprofile
    user_likes = one_user_likes(u.username)
    if request.method == "POST":
        user_form = UserSettingsForm(request.POST, user=request.user)  # we need user for email check

        if user_form.is_valid():
            email = user_form.cleaned_data['email']
            country = user_form.cleaned_data['country']
            about_me = user_form.cleaned_data['about_me']
            if email != u.email:
                u.email = email
                u.save()

            if up.country != country or up.about_me != about_me:  # if something changed then write else not
                up.country = country
                up.about_me = about_me
                up.save()
            messages.add_message(request, messages.INFO, 'You have successfully changed your settings')
            return HttpResponseRedirect(reverse("users:user_settings"))
    else:
        data = {'email': u.email, 'country': up.country, 'about_me': up.about_me}
        user_form = UserSettingsForm(initial=data, user=request.user)  # we need user for email check
    return render(request, template_name="users/user_settings.html", context={'form': user_form,
                                                                              'user_likes': user_likes,
                                                                              'user_profile': up})


@method_decorator(cache_control(no_cache=True, must_revalidate=True, no_store=True), name='dispatch')
class UserCommentListView(ListView):
    template_name = 'users/user_comments.html'

    def get_queryset(self):
        queryset = one_user_comments(self.kwargs['username']) #because we need ratio for the comments
        return queryset

    def get_context_data(self, **kwargs):
        context = super(UserCommentListView, self).get_context_data(**kwargs)
        up = get_object_or_404(UserProfile, user__username=self.kwargs['username'])
        context['user_profile'] = up
        context['user_likes'] = one_user_likes(self.kwargs['username'])
        is_followed = 0
        if self.request.user.is_authenticated():
            try:
                check_rel = Relationship.objects.get(from_person_id=self.request.user.userprofile.id,
                                  to_person_id=up.id, rel_status=1)
            except Relationship.DoesNotExist:
                check_rel = None

            if check_rel is not None:
                is_followed = 1

        context['is_followed'] = is_followed
        return context

@login_required()
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def change_password(request):
    user_likes = one_user_likes(request.user.username)
    up = request.user.userprofile
    return password_change(request, template_name='users/user_password_change.html',
        post_change_redirect='users:to_settings',
        password_change_form=PasswordChangeForm,
        extra_context={'pass': True, 'user_likes': user_likes, "user_profile": up})  #likes and profile for sub_menu


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def to_settings(request):
    messages.add_message(request, messages.INFO, 'You have successfully changed your password')
    return HttpResponseRedirect(reverse('users:user_settings'))


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def ajaxfollowing(request):
    if "type" in request.POST:
        if request.POST.get('type') == '1':
            insert_following(request.user.userprofile.id, request.POST.get('user_id'))
        else:
            delete_following(request.user.userprofile.id, request.POST.get('user_id'))

        response_dict = {}
        return HttpResponse(json.dumps(response_dict), content_type='application/javascript')
    else:
        return render(request, 'users/user_details.html', context_instance=RequestContext(request))