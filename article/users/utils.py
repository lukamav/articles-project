from django.db import connection


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]


def one_user_likes(username):
    query = ("select username, num_of_likes, num_of_dislikes, "
    "ROUND((num_of_likes / GREATEST((num_of_likes + num_of_dislikes), 1)),2) as ratio "
    "from ( "
    "select username, IFNULL(sum(likes), 0) as num_of_likes, IFNULL(sum(dislikes),0) as num_of_dislikes "
    "from sth_comment inner join auth_user on sth_comment.user_id = auth_user.id "
    "where username = %s "
    ") r ")

    cursor = connection.cursor()
    cursor.execute(query, [username])
    return dictfetchall(cursor)


def one_user_comments(username):
    query = ("select sth_comment.id, comment_headline, sth_comment.content, comment_date, spam_count, "
        "article_headline, likes, dislikes, username, "
        "ROUND((likes / greatest((likes + dislikes),1)),2) as ratio "
        "from sth_comment "
        "INNER JOIN auth_user on auth_user.id = sth_comment.user_id "
        "INNER JOIN sth_article on sth_article.id = sth_comment.article_id "
        "WHERE username = %s ")

    cursor = connection.cursor()
    cursor.execute(query, [username])
    return dictfetchall(cursor)


def insert_following(follower, followed):
    query = ('insert into users_relationship (rel_status, from_person_id, to_person_id) '
             'VALUES(%s, %s, %s)')

    cursor = connection.cursor()
    cursor.execute(query, [1, follower, followed])


def delete_following(follower, followed):
    query = 'DELETE FROM users_relationship WHERE rel_status = %s AND from_person_id = %s AND to_person_id = %s'

    cursor = connection.cursor()
    cursor.execute(query, [1, follower, followed])
