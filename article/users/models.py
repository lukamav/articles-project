from django.db import models
from settings import base
from django.contrib.auth.models import User
from django_countries.fields import CountryField
from django.core.urlresolvers import reverse


class UserProfile(models.Model):
    user = models.OneToOneField(base.AUTH_USER_MODEL)
    about_me = models.TextField()
    country = CountryField(blank_label='(select country)')
    relationship = models.ManyToManyField('self', through='Relationship', symmetrical=False)

    def __str__(self):
        return self.user.username


class Relationship(models.Model):
    RELATIONSHIP_FOLLOWING = 1
    RELATIONSHIP_BLOCKED = 2
    RELATIONSHIP_STATUSES = (
        (RELATIONSHIP_FOLLOWING, 'Following'),
        (RELATIONSHIP_BLOCKED, 'Blocked'),
    )

    from_person = models.ForeignKey(UserProfile, related_name='from_people')
    to_person = models.ForeignKey(UserProfile, related_name='to_people')
    rel_status = models.IntegerField(choices=RELATIONSHIP_STATUSES)