from django import forms
from django_countries import countries
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


COUNTRY_CHOICES = [('', 'Select country')] + list(countries)


class UserSettingsForm(forms.Form):
    email = forms.EmailField()
    country = forms.ChoiceField(choices=COUNTRY_CHOICES)
    about_me = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(UserSettingsForm, self).__init__(*args, **kwargs)

    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.

        """
        qs = User.objects.filter(email__iexact=self.cleaned_data['email'])
        if len(qs) == 1 and qs[0].email != self.user.email:
            raise forms.ValidationError(
                _("This email address is already in use. Please supply a different email address."))
        return self.cleaned_data['email']
