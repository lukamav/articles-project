from django.test import TestCase
from django.contrib.auth.models import User
from django.contrib import auth
from .models import *
from .forms import UserSettingsForm
from django.contrib.auth.forms import PasswordChangeForm
from django.core.urlresolvers import reverse
from .factories import UserFactory, ArticleFactory, CommentFactory
from sth.models import Comment, Article
from django.core import management
import os
import re



def test_data():
    users = []
    for i in range(10):
        users.append(UserFactory())
    Relationship.objects.create(from_person=users[0].userprofile, to_person=users[2].userprofile, rel_status=1)
    Relationship.objects.create(from_person=users[0].userprofile, to_person=users[3].userprofile, rel_status=1)

    a1 = ArticleFactory()
    a2 = ArticleFactory(category="culture")
    comments = []
    # user0 has 2 comments that were rated 0 times, user9 doesn't have any comments
    for a in (a1, a2):
        for u in users[:9]:
            if u.username == "user0":
                comments.append(CommentFactory(article=a, user=u, likes=0, dislikes=0))
            else:
                comments.append(CommentFactory(article=a, user=u))


class UserDetailsTest(TestCase):

    def setUp(self):
        UserFactory.reset_sequence()
        CommentFactory.reset_sequence()
        test_data()

    def tearDown(self):
        image_dir = os.path.join(base.MEDIA_ROOT, 'article')
        regex = re.compile(r'^example.*$')
        for file in os.listdir(image_dir):
            if regex.search(file):
                os.remove(os.path.join(image_dir, file))

    def test_existing_user(self):
        response = self.client.get(reverse("users:user_details", kwargs={"username": "user0"}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["user_profile"].country, "SI")

    def test_non_existing_user(self):
        response = self.client.get(reverse("users:user_details", kwargs={"username": "nonexistent"}))
        self.assertEqual(response.status_code, 404)

    def test_following_anonymous_user(self):
        response = self.client.get(reverse("users:user_details", kwargs={"username": "user0"}))
        self.assertEqual(response.context['is_followed'], 0)
        self.assertNotContains(response, '<input id="followed" type="hidden" name="followed"')

    def test_following_logged_user_not_following(self):
        logged_in = self.client.login(username="user0", password="password1")
        self.assertEqual(logged_in, True)
        if logged_in:
            response = self.client.get(reverse("users:user_details", kwargs={"username": "user5"}))
            self.assertEqual(response.context['is_followed'], 0)
            self.assertContains(response, '<input id="followed" type="hidden" name="followed" value="0">')

    def test_following_logged_user_following(self):
        logged_in = self.client.login(username="user0", password="password1")
        self.assertEqual(logged_in, True)
        if logged_in:
            response = self.client.get(reverse("users:user_details", kwargs={"username": "user2"}))
            self.assertEqual(response.context['is_followed'], 1)
            self.assertContains(response, '<input id="followed" type="hidden" name="followed" value="1">')

    def test_user_likes_no_comments(self):
        response = self.client.get(reverse("users:user_details", kwargs={"username": "user9"}))
        self.assertEqual(response.context["user_likes"][0]["num_of_likes"], 0)
        self.assertEqual(response.context["user_likes"][0]["num_of_dislikes"], 0)
        self.assertEqual(response.context["user_likes"][0]["ratio"], 0)

    def test_user_likes_comments_not_rated(self):
        response = self.client.get(reverse("users:user_details", kwargs={"username": "user0"}))
        self.assertEqual(response.context["user_likes"][0]["num_of_likes"], 0)
        self.assertEqual(response.context["user_likes"][0]["num_of_dislikes"], 0)
        self.assertEqual(response.context["user_likes"][0]["ratio"], 0)

    def test_user_likes(self):
        response = self.client.get(reverse("users:user_details", kwargs={"username": "user8"}))
        self.assertEqual(response.context["user_likes"][0]["num_of_likes"], 27)
        self.assertEqual(response.context["user_likes"][0]["num_of_dislikes"], 9)
        self.assertEqual(response.context["user_likes"][0]["ratio"], 0.75)

    def test_user_profile(self):
        response = self.client.get(reverse("users:user_details", kwargs={"username": "user0"}))
        self.assertEquals(response.context["user_profile"].user.username, "user0")


class UserCommentsTest(TestCase):
    def setUp(self):
        UserFactory.reset_sequence()
        CommentFactory.reset_sequence()
        test_data()

    def tearDown(self):
        image_dir = os.path.join(base.MEDIA_ROOT, 'article')
        regex = re.compile(r'^example.*$')
        for file in os.listdir(image_dir):
            if regex.search(file):
                os.remove(os.path.join(image_dir, file))

    def test_no_comments(self):
        response = self.client.get(reverse("users:user_comments", kwargs={"username": "user9"}))
        self.assertEqual(len(response.context["object_list"]), 0)
        self.assertContains(response, "No comments to show")

    def test_two_comments(self):
        response = self.client.get(reverse("users:user_comments", kwargs={"username": "user0"}))
        self.assertEqual(len(response.context["object_list"]), 2)
        self.assertNotContains(response, "No comments to show")


class UserSettingsTest(TestCase):

    def setUp(self):
        UserFactory.reset_sequence()
        CommentFactory.reset_sequence()
        test_data()

    def tearDown(self):
        image_dir = os.path.join(base.MEDIA_ROOT, 'article')
        regex = re.compile(r'^example.*$')
        for file in os.listdir(image_dir):
            if regex.search(file):
                os.remove(os.path.join(image_dir, file))

    def test_anonymous_user_no_redirect(self):
        response = self.client.get(reverse("users:user_settings"))
        self.assertEqual(response.status_code, 302)

    def test_anonymous_user_redirect_to_login(self):
        response = self.client.get(reverse("users:user_settings"), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, reverse("accounts:login") + '?next=' + reverse("users:user_settings"))

    def test_initial_data(self):
        logged_in = self.client.login(username="user0", password="password1")
        self.assertEqual(logged_in, True)
        if logged_in:
            response = self.client.get(reverse("users:user_settings"))
            self.assertEqual(response.context["form"]["email"].value(), response.context["user"].email)
            self.assertEqual(response.context["form"]["about_me"].value(), response.context["user"].userprofile.about_me)
            self.assertEqual(response.context["form"]["country"].value(), response.context["user"].userprofile.country)
            self.assertEqual(response.context["form"].is_valid(), False)
            self.assertEqual(response.context["form"].is_bound, False)

    def test_successful_form_submission(self):
        logged_in = self.client.login(username="user0", password="password1")
        self.assertEqual(logged_in, True)
        if logged_in:
            response = self.client.post(reverse("users:user_settings"), {"about_me": "neki", "email": "user0@email.com",
                                                                         "country": "FR"}, follow=True)
            self.assertRedirects(response, reverse("users:user_settings"))
            self.assertEqual(response.context["form"].is_valid(), False)
            self.assertEqual(response.context["form"].is_bound, False)

    def test_unsuccessful_form_submission(self):
        logged_in = self.client.login(username="user0", password="password1")
        self.assertEqual(logged_in, True)
        if logged_in:
            response = self.client.post(reverse("users:user_settings"), {"about_me": "neki", "email": "",
                                                                         "country": "FR"}, follow=True)
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.context["form"].is_bound, True)
            self.assertEqual(response.context["form"].is_valid(), False)


class UserSettingsFormTest(TestCase):

    def setUp(self):
        UserFactory.reset_sequence()
        CommentFactory.reset_sequence()
        test_data()

    def tearDown(self):
        image_dir = os.path.join(base.MEDIA_ROOT, 'article')
        regex = re.compile(r'^example.*$')
        for file in os.listdir(image_dir):
            if regex.search(file):
                os.remove(os.path.join(image_dir, file))

    def test_empty_form(self):
        logged_in = self.client.login(username="user5", password="password1")
        u = User.objects.get(id=self.client.session["_auth_user_id"])  #u=auth.get_user(self.client) -> hack
        data = {"email": '', "about_me": '', "country": ''}
        form = UserSettingsForm(data, user=u)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["email"], ['This field is required.'])
        self.assertEqual(form.errors["country"], ['This field is required.'])
        self.assertNotIn("about_me", form.errors.keys())

    def test_wrong_email_format(self):
        logged_in = self.client.login(username="user5", password="password1")
        u = User.objects.get(id=self.client.session["_auth_user_id"])  #u=auth.get_user(self.client) -> hack
        data = {"email": 'somethinggmail.com', "about_me": 'Something', "country": 'SI'}
        form = UserSettingsForm(data, user=u)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["email"], ['Enter a valid email address.'])

    def test_same_mail_as_in_database(self):
        logged_in = self.client.login(username="user5", password="password1")
        u = User.objects.get(id=self.client.session["_auth_user_id"])  #u=auth.get_user(self.client) -> hack
        data = {"email": 'user5@email.com', "about_me": 'Something', "country": 'SI'}
        form = UserSettingsForm(data, user=u)
        self.assertTrue(form.is_valid())

    def test_already_taken_email(self):
        logged_in = self.client.login(username="user5", password="password1")
        u = User.objects.get(id=self.client.session["_auth_user_id"])  #u=auth.get_user(self.client) -> hack
        data = {"email": 'user0@email.com', "about_me": 'Something', "country": 'SI'}
        form = UserSettingsForm(data, user=u)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["email"],
                         ["This email address is already in use. Please supply a different email address."])

class UserPasswordChangeTest(TestCase):

    def setUp(self):
        UserFactory.reset_sequence()
        CommentFactory.reset_sequence()
        test_data()

    def tearDown(self):
        image_dir = os.path.join(base.MEDIA_ROOT, 'article')
        regex = re.compile(r'^example.*$')
        for file in os.listdir(image_dir):
            if regex.search(file):
                os.remove(os.path.join(image_dir, file))

    def test_anonymous_user_no_redirect(self):
        response = self.client.get(reverse("users:password_change"))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse("accounts:login") + '?next=' + reverse("users:password_change"))

    def test_anonymous_user_redirect_to_login(self):
        response = self.client.get(reverse("users:password_change"), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, reverse("accounts:login") + '?next=' + reverse("users:password_change"))

    def test_successful_password_change_no_redirect(self):
        logged_in = self.client.login(username="user0", password="password1")
        data = {"old_password": "password1", "new_password1": "password2", "new_password2": "password2"}
        if logged_in:
            response = self.client.post(reverse("users:password_change"), data)
            self.assertEqual(response.status_code, 302)

    def test_successful_password_change_redirect(self):
        logged_in = self.client.login(username="user0", password="password1")
        data = {"old_password": "password1", "new_password1": "password2", "new_password2": "password2"}
        if logged_in:
            response = self.client.post(reverse("users:password_change"), data, follow=True)
            self.assertEqual(response.status_code, 200)

    def test_unsuccessful_password_change(self):
        logged_in = self.client.login(username="user0", password="password1")
        data = {"old_password": "password", "new_password1": "password2", "new_password2": "password2"}
        if logged_in:
            response = self.client.post(reverse("users:password_change"), data, follow=True)
            self.assertFalse(response.context['form'].is_valid())
            self.assertEqual(response.status_code, 200)


class UserPasswordChangeFormTest(TestCase):

    def setUp(self):
        UserFactory.reset_sequence()
        CommentFactory.reset_sequence()
        test_data()

    def tearDown(self):
        image_dir = os.path.join(base.MEDIA_ROOT, 'article')
        regex = re.compile(r'^example.*$')
        for file in os.listdir(image_dir):
            if regex.search(file):
                os.remove(os.path.join(image_dir, file))

    def test_empty_form(self):
        logged_in = self.client.login(username="user0", password="password1")
        data = {"old_password": "", "new_password1": "", "new_password2": ""}
        form = PasswordChangeForm(user=User.objects.get(id=self.client.session["_auth_user_id"]), data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["old_password"], ['This field is required.'])
        self.assertEqual(form.errors["new_password1"], ['This field is required.'])
        self.assertEqual(form.errors["new_password2"], ['This field is required.'])

    def test_wrong_old_password(self):
        logged_in = self.client.login(username="user0", password="password1")
        data = {"old_password": "wrongpassword", "new_password1": "password2", "new_password2": "password2"}
        form = PasswordChangeForm(user=User.objects.get(id=self.client.session["_auth_user_id"]), data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["old_password"],
                         ['Your old password was entered incorrectly. Please enter it again.'])

    def test_non_matching_passwords(self):
        logged_in = self.client.login(username="user0", password="password1")
        data = {"old_password": "password1", "new_password1": "password2", "new_password2": "password3"}
        form = PasswordChangeForm(user=User.objects.get(id=self.client.session["_auth_user_id"]), data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["new_password2"],
                         ["The two password fields didn't match."])

    def test_too_short_non_matching_passwords(self):
        logged_in = self.client.login(username="user0", password="password1")
        data = {"old_password": "password1", "new_password1": "test", "new_password2": "test1"}
        form = PasswordChangeForm(user=User.objects.get(id=self.client.session["_auth_user_id"]), data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["new_password2"],
                         ["The two password fields didn't match."])

    def test_to_short_password(self):
        logged_in = self.client.login(username="user0", password="password1")
        data = {"old_password": "password1", "new_password1": "test", "new_password2": "test"}
        form = PasswordChangeForm(user=User.objects.get(id=self.client.session["_auth_user_id"]), data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["new_password2"],
                         ["This password is too short. It must contain at least 6 characters."])

    def test_successful_password_change(self):
        logged_in = self.client.login(username="user0", password="password1")
        data = {"old_password": "password1", "new_password1": "password2", "new_password2": "password2"}
        form = PasswordChangeForm(user=User.objects.get(id=self.client.session["_auth_user_id"]), data=data)
        self.assertTrue(form.is_valid())
