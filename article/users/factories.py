__author__ = 'jest'
import factory
from django.contrib.auth import get_user_model
from .models import *
from sth.models import Comment, Article
from django.utils import timezone
import datetime

class UserProfileFactory(factory.DjangoModelFactory):
    class Meta:
        model = UserProfile

    user = None
    about_me = "about me about me"
    country = "SI"


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = get_user_model()

    username = factory.Sequence(lambda n: "user%s" % str(n))
    email = factory.LazyAttribute(lambda u: "%s@email.com" % u.username)
    password = factory.PostGenerationMethodCall('set_password', 'password1')
    profile = factory.RelatedFactory(UserProfileFactory, "user")


class ArticleFactory(factory.DjangoModelFactory):
    class Meta:
        model = Article

    article_headline = factory.Sequence(lambda n: "Topic%s" % str(n))
    article_date = timezone.now() - datetime.timedelta(days=60)
    image = factory.django.ImageField()
    description = factory.LazyAttribute(lambda t: "%s description" % t.article_headline)
    content = "Lorem ipsum ipsum lorem"
    category = "sport"


class CommentFactory(factory.DjangoModelFactory):
    class Meta:
        model = Comment

    article = None
    user = None
    comment_headline = factory.Sequence(lambda n: "Comment%s" % str(n))
    content = "Lorem " * 50
    comment_date = factory.Sequence(lambda n: timezone.now() - datetime.timedelta(days=n))
    likes = factory.Sequence(lambda n: n + 1)
    dislikes = factory.Sequence(lambda n: (17 - n))
    spam_count = 0

