from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(?P<username>[\w.@+-]+)/about/$', views.user_details, name="user_details"),
    url(r'^settings/$', views.settings_view, name="user_settings"),
    url(r'^(?P<username>[\w.@+-]+)/comments/$', views.UserCommentListView.as_view(), name="user_comments"),
    url(r'^settings/password/change/$', views.change_password, name='password_change'),

    url(r'^to/settings/$', views.to_settings, name="to_settings"),
    url(r'^ajaxfollowing/$', views.ajaxfollowing,),
]
